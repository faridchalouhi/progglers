import sys
import subprocess
import re

if (len(sys.argv) != 3):
    print "Usage: ", sys.argv[0], " <command_path> <number_of_tries>"
    exit()
command = sys.argv[1]
number_of_tries = int(sys.argv[2])

results = {}

for i in xrange(1,number_of_tries):
    #print "Try #", i
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = []
    lastline = ''
    for line in p.stdout.readlines():
        lastline = line
        output.append(line)
        #print line,
    retval = p.wait()
    m = re.findall(r'score=(\d+)', lastline)
    score = int(m[0])
    #print " score = ", score
    results[score] = output

bestScore = max(k for k, v in results.iteritems() if v != 0)
#print "Final version"
print ''.join(results[bestScore])