import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;


public class DFSRouteSearcher {
	
	public static void main(String[] args) throws Exception {
		Context context = new Context();
		Junction initialJunction = ReadStreets.readStreetFile("../paris_54000.txt", context);
		DFSRouteSearcher searcher = new DFSRouteSearcher();
		Route routes[] = searcher.search(context, initialJunction);
		System.out.println(routes);
	}
	
	Route[] search(Context context, Junction initialJunction) {
		Route[] routes = new Route[context.vehiculeCount]; 
		routes[0] = getBestRoute(initialJunction, context.availableTime, new HashSet<Street>());
		return routes;
	}
	
	static Route getBestRoute(Junction i, int remainingTime, Set<Street> visitedStreets) {
		System.out.println("Remaining time: " + remainingTime);
		if (remainingTime < 0) return null;
		Route bestRoute = null;
		Street bestStreet = null;
		for (Street street : i.streets) {
			if (visitedStreets.contains(street)) continue; // Optimization(?): never twice the same street
			Junction destination = (street.startJunction == i) ? street.endJunction : street.startJunction;
			Set<Street> newVisitedStreets = new HashSet<Street>(visitedStreets);
			newVisitedStreets.add(street);
			Route r = getBestRoute(destination, remainingTime - street.cost, newVisitedStreets);
			if (r != null) {
				if (r.visitedStreets.contains(street) || visitedStreets.contains(street)) {
					r.score += 0; // Street already visited :(
				} else {
					r.score += street.length;
				}
				if (bestRoute == null || r.score > bestRoute.score) {
					bestRoute = r;
					bestStreet = street;
				}
			}
		}
		if (bestRoute == null) {
			bestRoute = new Route();
		}
		bestRoute.intersections.add(i);
		bestRoute.visitedStreets.add(bestStreet);
		return bestRoute;
	}
}
