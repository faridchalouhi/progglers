import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;


public class GluttonRouteSearcher {
	
	public static void main(String[] args) throws Exception {
		Context context = new Context();
		Junction initialJunction = ReadStreets.readStreetFile("../paris_54000.txt", context);
		GluttonRouteSearcher searcher = new GluttonRouteSearcher();
		List<Junction> routes[] = searcher.search(context, initialJunction);
		WriteMoves.writeMoves("moves.txt", routes);
	}
	
	List<Junction>[] search(Context context, Junction initialJunction) {
		List<Junction>[] routes = new List[context.vehiculeCount]; 
		Set<Street> visitedStreets = new HashSet<Street>();
		for (int i = 0 ; i < context.vehiculeCount ; i++) {
			System.out.println("Vehicle " + i);
			routes[i] = getBestRoute(initialJunction, context.availableTime, visitedStreets);
			System.out.println(routes[i]);
		}
		return routes;
	}
	
	static List<Junction> getBestRoute(Junction startJunction, int remainingTime, Set<Street> visitedStreets) {
		List<Junction> route = new ArrayList<Junction>();
		route.add(startJunction);
		while (remainingTime > 0) {
			int streetCount = startJunction.streets.size();
			double value[] = new double[streetCount];
			for (int i = 0 ; i < streetCount ; i++) {
				Street street = startJunction.streets.get(i);
				value[i] = (visitedStreets.contains(street)) ? 0.0 : street.length / street.cost;
			}
			int maxPosition = getMaxPosition(value);
			if (maxPosition == -1) {
				maxPosition = (int) (Math.random() * value.length);
			}
			
			Street street = startJunction.streets.get(maxPosition);
			Junction destination = (street.startJunction == startJunction) ? street.endJunction : street.startJunction;
			if (remainingTime > street.cost) {
				remainingTime -= street.cost;
				startJunction = destination;
				route.add(destination);
				visitedStreets.add(street);
				System.out.println("Remaining " + remainingTime);
			} else {
				break;
			}
		}
		return route;
	}

	private static int getMaxPosition(double[] value) {
		double maxVal = Double.MIN_VALUE;
		int maxPos = -1;
		for (int i = 0 ; i < value.length ; i++) {
			if (value[i] > maxVal) {
				maxVal = value[i];
				maxPos = i;
			}
		}
		return maxPos;
	}
}
