import java.util.*;

public class Junction {
	public float lat;
	public float longi;
	public int index; // For identification
	
	public ArrayList<Street> streets = new ArrayList<Street>();

	public Junction(int index) {
		this.index = index;
	}

	public void addStreet(Street street) {
		streets.add(street);
	}

	public ArrayList<Street> getStreets() {
		return streets;
	}
	
	public String toString() {
		return "j"+index;
	}

}
