import java.util.*;
import java.io.*;

public class ReadStreets {

	public static void main(String[] args) throws Exception {
		readStreetFile("../paris_54000.txt", new Context());
	}

	static Junction readStreetFile(String pathname, Context context) throws Exception {
		Scanner scanner = null;
		scanner = new Scanner(new File(pathname));
		
		int number_of_junctions = scanner.nextInt();
		int number_of_streets = scanner.nextInt();
		context.availableTime = scanner.nextInt();
		context.vehiculeCount = scanner.nextInt();
		int initial_junction = scanner.nextInt();
		
		Junction[] junctions = new Junction[number_of_junctions];
		Junction initialJunction = null;
		Street[] streets = new Street[number_of_streets];
		
		// It is necessary to skip one line here...
		scanner.nextLine();
		
		String[] coordinates;
		for(int i=0 ; i < number_of_junctions ; i++)
		{
			junctions[i] = new Junction(i);
			coordinates = scanner.nextLine().split(" ");
			junctions[i].lat =  Float.parseFloat(coordinates[0]);
			junctions[i].longi =  Float.parseFloat(coordinates[1]);
			if (i == initial_junction) {
				initialJunction = junctions[i];
			}
		}
		
		for(int i=0 ; i < number_of_streets ; i++)
		{
			streets[i] = new Street();
			int startIndex = scanner.nextInt();
			int endIndex = scanner.nextInt();
			streets[i].startJunction = junctions[startIndex];
			streets[i].endJunction = junctions[endIndex];
			
			streets[i].direction = scanner.nextInt();
			streets[i].cost = scanner.nextInt();
			streets[i].length = scanner.nextInt();
			
			junctions[startIndex].addStreet(streets[i]);
			
			if(streets[i].direction == 2)
				junctions[endIndex].addStreet(streets[i]);
		}
		
		return initialJunction;
	}
}
