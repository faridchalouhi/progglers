public class Street {
	public static final int ONE_WAY = 1;
	public static final int BIDIRECTIONAL = 2;
	
	public Junction startJunction;
	public Junction endJunction;
	
	public int direction;
	public int length;
	public int cost;
	
	public Street() {}
	
	public Street(Junction startJunction, Junction endJunction, int cost, int length, int direction) {
		this.startJunction = startJunction;
		this.endJunction = endJunction;
		this.cost = cost;
		this.length = length;
		this.direction = direction;
		
		startJunction.addStreet(this);
		if (direction == BIDIRECTIONAL) {
			endJunction.addStreet(this);
		}
	}
	
	public String toString() {
		return startJunction+"-"+endJunction;
	}
}
