import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;


public class WriteMoves {

	public static void writeMoves(String pathname, List<Junction>[] routes) throws Exception {
		PrintWriter out = new PrintWriter(pathname);
		out.println(routes.length);
	    try {
	        for (int i = 0 ; i < routes.length ; i++) {
	        	List<Junction> route = routes[i];
	            out.println(route.size());
	            for (Junction junction : route) {
					out.println(junction.index);
				}
	            
	        }
	    } finally {
	        out.close();
	    }
	}

}
