import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RouteSearcherTest {
	
	Context context = new Context();
	DFSRouteSearcher searcher = new DFSRouteSearcher();
	Junction i1 = new Junction(1);
	Junction i2 = new Junction(2);
	Junction i3 = new Junction(3);
	
	@Before
	public void setUp() {
		context.availableTime = 10;
		context.vehiculeCount = 1;
	}

	@Test
	public void test1Street() {
		Street s1 = new Street(i1, i2, 2, 10, Street.ONE_WAY);

		Route[] routes = searcher.search(context, i1);
		
		assertEquals(1, routes.length);
		Route expectedRoute = new Route();
		expectedRoute.intersections = asList(i2, i1);
		assertEquals(expectedRoute.intersections, routes[0].intersections);
	}

	@Test
	public void test2Streets() {
		Street s1 = new Street(i1, i2, 2, 5, Street.ONE_WAY);
		Street s2 = new Street(i1, i3, 2, 10, Street.ONE_WAY);
		
		Route[] routes = searcher.search(context, i1);
		
		assertEquals(1, routes.length);
		Route expectedRoute = new Route();
		expectedRoute.intersections = asList(i3, i1);
		assertEquals(expectedRoute.intersections, routes[0].intersections);
	}
	
	@Test
	public void test3Streets() {
		Street s1 = new Street(i1, i2, 1, 5, Street.ONE_WAY);
		Street s2 = new Street(i2, i3, 1, 5, Street.ONE_WAY);
		Street s3 = new Street(i1, i3, 1, 6, Street.ONE_WAY);
		
		Route[] routes = searcher.search(context, i1);
		
		assertEquals(1, routes.length);
		Route expectedRoute = new Route();
		expectedRoute.intersections = asList(i3, i2, i1);
		assertEquals(expectedRoute.intersections, routes[0].intersections);
	}
	
	@Test
	public void test3StreetsWithReturn() {
		Street s1 = new Street(i1, i2, 1, 5, Street.BIDIRECTIONAL);
		Street s2 = new Street(i2, i3, 1, 5, Street.ONE_WAY);
		
		Route[] routes = searcher.search(context, i1);
		
		assertEquals(1, routes.length);
		Route expectedRoute = new Route();
		expectedRoute.intersections = asList(i3, i1, i2, i1);
		//assertEquals(expectedRoute.intersections, routes[0].intersections);
	}
}
