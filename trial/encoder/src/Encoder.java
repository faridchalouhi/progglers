import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Encoder {

	public static void main(String[] args) throws Exception {
		Encoder encoder = new Encoder();
		boolean[][] image = encoder.loadImageFromFile("../doodle.txt");
		
		// List<String> instructions = naiveConvertionToInstructions(image);
		List<String> instructions = encoder.convertionToInstructions(image);
		PrintWriter out = new PrintWriter("instructions.txt");
		out.print(instructions.size());
		out.print("\n");
		for (String instruction : instructions) {
			out.print(instruction);
			out.print("\n");
		}
		out.close();
	}

	private List<String> convertionToInstructions(boolean[][] image) {
		int[][] squareSize = new int[image.length][image[0].length];
		List<String> instructions = new ArrayList<String>();
		for (int r = 0 ; r < image.length ; r++) {
			for (int c = 0 ; c < image[0].length ; c++) {
				if (image[r][c] == true) {
					squareSize[r][c] = findLargestSquare(image, r, c);
				}
			}
		}
		Square s;
		while((s = getMaxSizeSquare(squareSize)).size != 0) {
			instructions.add("PAINTSQ " + s.r + " " + s.c + " " + s.size);
			removeSquare(squareSize, s);
		}
		return instructions;
	}

	private void removeSquare(int[][] squareSize, Square s) {
		for (int r = s.r - s.size ; r <= s.r + s.size ; r++) {
			for (int c = s.c - s.size ; c <= s.c + s.size ; c++) {
				squareSize[r][c] = 0;
			}
		}
	}

	private Square getMaxSizeSquare(int[][] squareSize) {
		Square maxSquare = new Square();
		maxSquare.size = 0;
		for (int r = 0 ; r < squareSize.length ; r++) {
			for (int c = 0 ; c < squareSize[0].length ; c++) {
				if (squareSize[r][c] > maxSquare.size) {
					maxSquare.r = r;
					maxSquare.c = c;
					maxSquare.size = squareSize[r][c];
				}
			}
		}
		System.out.println("getMaxSizeSquare=" + maxSquare);
		return maxSquare;
	}

	int findLargestSquare(boolean[][] image, int r, int c) {
		int size = 0;
		
		while(true) {
			size++;
			if (r - size < 0 || r + size >= image.length || c - size < 0 || c + size >= image[0].length) break;
			if (checkFullSquare(image, r, c, size) == false) {
				break;
			}
		}
		--size;
		return size;
	}
	
	boolean checkFullSquare(boolean[][] image, int r, int c, int size) {
		for (int x = r-size ; x <= r+size ; x++) {
			for (int y = c-size ; y <= c+size ; y++) {
				if (image[x][y] == false) return false;
			}
		}
		return true;
	}
	
	static List<String> naiveConvertionToInstructions(boolean[][] image) {
		List<String> instructions = new ArrayList<String>();
		for (int r = 0 ; r < image.length ; r++) {
			for (int c = 0 ; c < image[0].length ; c++) {
				if (image[r][c]) {
					String instruction = "PAINTSQ " + r + " " + c + " 0\n";
					instructions.add(instruction);
					System.out.println(instruction);
				}
			}
		}
		return instructions;
	}
	
	boolean[][] loadImageFromFile(String pathname) throws IOException {
		String imageDescription = readFile(pathname);
		boolean[][] image = loadImage(imageDescription);
		return image;
	}
	
	boolean[][] loadImage(String imageDescription) {
		String[] descriptionLines = imageDescription.split("\n");
		String[] imageSize = descriptionLines[0].split(" ");
		int height = Integer.parseInt(imageSize[0]);
		int width = Integer.parseInt(imageSize[1]);
		
		boolean[][] image = new boolean[height][width];
		for (int r = 0 ; r < height ; r++) {
			for (int c = 0 ; c < width ; c++) {
				image[r][c] = descriptionLines[r+1].charAt(c) == '#' ? true : false;
			}
		}
		
		return image;
	}
	
	private String readFile(String pathname) throws IOException {
	    File file = new File(pathname);
	    StringBuilder fileContents = new StringBuilder((int)file.length());
	    Scanner scanner = new Scanner(file);
	    String lineSeparator = System.getProperty("line.separator");

	    try {
	        while(scanner.hasNextLine()) {        
	            fileContents.append(scanner.nextLine() + lineSeparator);
	        }
	        return fileContents.toString();
	    } finally {
	        scanner.close();
	    }
	}
}
