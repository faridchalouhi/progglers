import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class EncoderTest {

	Encoder e;
	
	@Before
	public void setUp() {
		e = new Encoder();
	}
	
	@Test
	public void checkFullSquare() {
		assertTrue(e.checkFullSquare(new boolean[][] {{true}}, 0, 0, 0));
	}

}
