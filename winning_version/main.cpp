
#include "utils.cpp"


car_t *car;

int n_intersections,
	n_street,
	time_remaining,
	n_car,
	source;


position_t *inter;
street_t *street;

std::list<dest_t> *dests;

// get the distance Elliptic
double dist(int voiture, int a)
{
    // utilise la distance ellipsoidale de vincenty
    double R = 6371000;
    double PI = 3.1415926;

    double d = 0;
    double tmp;
    tmp = inter[a].lon - interesting[voiture].lon;
    tmp *= tmp;
    d+=tmp;

    tmp = inter[a].lat - interesting[voiture].lat;
    tmp *= tmp;
    d+=tmp;

    return R*sqrt(d)/ 180.0*PI; // return the distance
}

// this is to set score
double setScores(int a, dest_t d) {
	return (1.0*d.doneA)/(1.0*d.price);
}

// list of cars
std::list<int> *car_strat;


/*
 * Set the car steps one by one
 */
void carSteps(int voiture, int dest, int cout, int rue) {
	if(car[voiture].time)  exit(1);

	car[voiture].time = cout;
	car[voiture].comming=car[voiture].position;
	car[voiture].position=dest;
	car_strat[voiture].push_back(dest);
	street[rue].parcourue = true;
}

// this to decide of the car selection
void decide_car(int id, int source, std::list<dest_t>& d, int back) {
	double max_np=0;
	int max_np_id=-1;
	int max_np_dest=-1;
	int max_np_cout=-1;
	int max_np_rue=-1;

	int r = rand()%d.size();
	int i = 0;
	int dest_rand = 0;
	int cout_rand = -1;
	int rue_rand = 0;
	dest_t max_np_d;
	for(auto& t: d) {

		if(t.a == back)
			continue;
		if(!street[t.rue].parcourue) {
			if(setScores(source, t) > max_np) {
				max_np = setScores(source, t);
				max_np_id = t.a;

				max_np_d = t;
			}
		}

		if(r == i) {
			dest_rand = t.a;
			cout_rand = t.price;
			rue_rand = t.rue;
		}
		++i;

	}
	if(max_np_id == -1) {
		carSteps(id, dest_rand, cout_rand, rue_rand);
	} else {
		carSteps(id, max_np_d.a, max_np_d.price, max_np_d.rue);
	}
}

// main fct
int main() {
	srand(time(NULL));

	// open the file
	FILE* fp = fopen("paris_54000.txt", "r");

	// get the value before parse
	fscanf(fp, "%d %d %d %d %d",
			&n_intersections,
			&n_street,
			&time_remaining,
			&n_car,
			&source);
	car = new car_t[n_car];

	// the car strat by here
	car_strat = new std::list<int>[n_car];
	for(int i=0; i<n_car; ++i) {
		car_strat[i].push_back(source);
		car[i].comming=-1;
		car[i].position=source;
	}


	inter = new position_t[n_intersections];

	dests = new std::list<dest_t>[n_intersections];


	for(int i=0; i<n_intersections; ++i) {
		fscanf(fp, "%f %f", &inter[i].latitude, &inter[i].longitude);
	}

	street = new street_t[n_street];

	for(int i=0; i<n_street; ++i) {
		int mono;

		// scan the file
		fscanf(fp, "%d %d %d %d %d", &street[i].a, &street[i].b, &mono, &street[i].price, &street[i].doneA);

		// set the way of directions
		street[i].monoDirection = mono == 1;
		dest_t tmp;

		// set the tmp
		tmp.a = street[i].b;
		tmp.price = street[i].price;
		tmp.doneA = street[i].doneA;
		tmp.rue = i;

		// include the destination
		dests[street[i].a].push_front(tmp);

		// if mono
		if(!street[i].monoDirection)
		{
			tmp.a = street[i].a;
			dests[street[i].b].push_front(tmp);
		}

	}
	fclose(fp);

	// if time remain
	while(--time_remaining)
	{

		// MANIPULATE ALL THE N CARS
		for(int i=0; i<n_car; ++i)
		{
			if(car[i].time) {
				car[i].time--;
				continue;
			}

			auto& d = dests[car[i].position];
			decide_car(i, car[i].position, d, car[i].comming);
		}
	}

	// this to set the total score
	int totalScore = 0;
	for(int i=0; i< n_street; ++i) {
		if(street[i].parcourue)
			totalScore += street[i].doneA;
	}

	std::cout<< "BestScore="+ totalScore;
}
