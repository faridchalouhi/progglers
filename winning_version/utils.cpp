#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <iostream>



typedef struct {
	float latitude;
	float longitude;
} position_t;

typedef struct {
	int a,b;
	bool monoDirection;
	int price;
	int doneA;

	bool parcourue;
} street_t;

typedef struct {
	int time;
	int position;
	int comming;
} car_t;

typedef struct {
	int a;
	int price;
	int doneA;
	int rue;
} dest_t;


position_t bestPoints[] = {
		{ 48.846577, 2.4002473},
		{ 48.864704, 2.3644626},
		{48.846836, 2.3694212 },
		{48.85427, 2.2832713},
		{48.88245, 2.3467174},
		{48.822296, 2.338629},
		{48.842632, 2.321999},
		{48.89631, 2.3595397}
};
